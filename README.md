# PROJECTE LLENGUATGE DE MARQUES

## AUTOR

Xavi Siles Pulido

## TEMÀTICA DE LA WEB

Informació detallada sobre productes esportius, específicament de bambes de bàsquet.

## DISTRIBUCIÓ DE PÀGINES

- (PÀG 1) Es podrà seleccionar entre les 3 marques més punteres del sector (Nike, Adidas i Under Armour), una pàgina de propers llençaments i una altre de recomanació sobre quina bamba comprar depenent de cada jugador.

- (PÀG 2) Informació específica de les sabates més venudes de la marca Nike, juntament amb el seu preu.

- (PÀG 3) Informació específica de les sabates més venudes de la marca Adidas, juntament amb el seu preu.

- (PÀG 4) Informació específica de les sabates més venudes de la marca Under Armour. juntament amb el seu preu.

- (PÀG 5) Propers llençaments de noves bambes i breu descripció hipotètica de les tecnologies que conté cada una i data hipotètica de llençament.

- (PAG 6) Pàgina on es recomana quina bamba comprar segons el perfil del client/jugador.

## CONTINGUT DE CADA PÀGINA

- (PÀG 1) En aquesta pàgina es farà una breu descripció del funcionament de la web, botons per seleccionar quina marca vol mirar el client i les pàgines de propers llençaments i de recomanació segons els jugadors.

- (PÀG 2/3/4) Pàgina amb scroll vertical on es mostraràn imatges de les diferents bambes amb una fitxa tècnica de cada una. 

- (PÀG 5) Pàgina similar a les anteriors, aquí es separaran les marques en la mateixa pàgina, serà de tipus scroll i amb imatges de les sabates i els seus detalls tècnics + descripció.

- (PÀG 6) Es donaran varies opcions de perfils de jugadors amb les bambes recomanades per aquell client/jugador. 